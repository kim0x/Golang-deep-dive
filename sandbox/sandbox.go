package main

import "fmt"

func main() {
	x := int8(127)
	y := int8(1)
	//int8 maximum ของมันคือ 127 ถ้าเราบวกไปอีก 1 จะได้ 128 ใช่ไหม? แต่ว่า 128 มัน overflow lenght ของมันอยู่ที่ -128 จนถึง 127
	//ถ้าเราบวกไปอีก 1 หละ มัน represent 128 ไม่ได้จะเกิดอะไรขึ้น
	fmt.Println(x + y) //-128
	fmt.Println(x + 2) //-127
	fmt.Println(x + 3) //-126

	//fmt.Println(int8(128)) - cannot convert 128 (untyped int constant) to type int8

	a := 127
	b := 2
	fmt.Println(int8(a + b))
	/*
		เพราะ a , b มันคือ runtime compiler มันไม่รู้ สมุติว่าเป็น variable ทีี่รับมาจากข้างนอก อาจจะผ่าน command มันก็จะไม่รู้ว่าเกิน lenght ของ int8 หรือเปล่า
		cast integer เป็น int8 จะเกิดอะไรขึ้น มันจะทำการ round กลับมา มันจะทำการ discuss bit อันที่สูงกว่าออกไป เลยได้ออกมาเป็น -128
	*/
	fmt.Printf("%b\n", 129) // 1000 0001 binary ของ 129
	z := 3.999
	fmt.Println(int(z)) // 3
}
